<?php

namespace App\Http\Controllers;


use App\Models\Payment;
use App\Models\Reserve;
use App\Models\Daily;
use Illuminate\Http\Request;


class ReserveController extends Controller
{
 /**
 * @OA\Get(
 *     path="/api/reserves",
 *     summary="Listar todas as reservas",
 *     description="Este endpoint retorna a lista de todas as reservas.",
 *     @OA\Response(
 *         response=200,
 *         description="Operação bem-sucedida",
 *     )
 * )
 */


    public function list()
    {
        $reserves = Reserve::all();

        return response()->json(['data' => $reserves], 200);
    }


/**
 * @OA\Post(
 *     path="/api/reserves",
 *     summary="Criar uma nova reserva",
 *     description="Este endpoint permite criar uma nova reserva com base nos dados fornecidos.",
 *     @OA\RequestBody(
 *         @OA\JsonContent(
 *             type="object",
 *             required={"hotel_id", "room_id", "checkIn", "checkOut"},
 *             @OA\Property(property="hotel_id", type="integer", description="ID do hotel"),
 *             @OA\Property(property="room_id", type="integer", description="ID da sala"),
 *             @OA\Property(property="checkIn", type="string", format="date", description="Data de check-in"),
 *             @OA\Property(property="checkOut", type="string", format="date", description="Data de check-out, deve ser posterior à data de check-in")
 *         )
 *     ),
 *     @OA\Response(response=201, description="Reserva criada com sucesso"),
 *     @OA\Response(response=400, description="Requisição inválida")
 * )
 */


    public function createReserve(Request $request)
    {
        $request->validate(Reserve::$rules);

        $reserve = Reserve::create($request->all());

        return response()->json(['message' => 'Reserve created successfully', 'data' => $reserve], 201);
    }
/**
 * @OA\Post(
 *     path="/api/reserves/{reserveId}/dailies",
 *     summary="Adicionar dailies a uma reserva",
 *     description="Este endpoint permite adicionar entradas de 'dailies' a uma reserva existente.",
 *     @OA\Parameter(
 *         name="reserveId",
 *         in="path",
 *         description="ID da reserva",
 *         required=true,
 *         @OA\Schema(type="integer")
 *     ),
 *     @OA\RequestBody(
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(
 *                 type="object",
 *                 required={"date"},
 *                 @OA\Property(property="date", type="string", format="date", description="Data da entrada 'dailies'")
 *             )
 *         )
 *     ),
 *     @OA\Response(response=201, description="Dailies adicionados à reserva com sucesso"),
 *     @OA\Response(response=400, description="Requisição inválida")
 * )
 */




    public function addDailies(Request $request, $reserveId)
    {
        $request->validate(Daily::$arrayRules);

        $reserve = Reserve::findOrFail($reserveId);

        foreach ($request->dailies as $dailyData) {
            $daily = new Daily($dailyData);
            $reserve->dailies()->save($daily);
        }

        return response()->json(['message' => 'Dailies added to reserve successfully'], 201);
    }

 
/**
 * @OA\Post(
 *     path="/api/reserves/{reserveId}/payments",
 *     summary="Adicionar pagamentos a uma reserva",
 *     description="Este endpoint permite adicionar pagamentos a uma reserva existente e atualizar o valor total da reserva.",
 *     @OA\Parameter(
 *         name="reserveId",
 *         in="path",
 *         description="ID da reserva",
 *         required=true,
 *         @OA\Schema(type="integer")
 *     ),
 *     @OA\RequestBody(
 *         @OA\JsonContent(
 *             type="array",
 *             @OA\Items(
 *                 type="object",
 *                 required={"method", "value"},
 *                 @OA\Property(property="method", type="integer", description="Método de pagamento"),
 *                 @OA\Property(property="value", type="number", format="float", description="Valor do pagamento")
 *             )
 *         )
 *     ),
 *     @OA\Response(response=201, description="Pagamentos adicionados à reserva com sucesso"),
 *     @OA\Response(response=400, description="Requisição inválida")
 * )
 */



    public function addPayments(Request $request, $reserveId)
    {
        $request->validate(Payment::$arrayRules);

        $reserve = Reserve::findOrFail($reserveId);

        foreach ($request->payments as $paymentData) {
            $payment = new Payment($paymentData);
            $reserve->payments()->save($payment);

            // Novo total da reserva
            $reserve->total += $payment->value;
            $reserve->save();
        }

        return response()->json(['message' => 'Payments added to reserve successfully'], 200);
    }
}
