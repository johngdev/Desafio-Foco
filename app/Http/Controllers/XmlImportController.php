<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Reserve;
use App\Models\Guest;
use App\Models\Daily;
use App\Models\Payment;
use App\Models\Room;

class XmlImportController extends Controller
{
    public function importHotels(/* Request $request */)
    {
        // $xml = new \SimpleXMLElement($request->getContent());

        $xmlContent = file_get_contents('xmls/hotels.xml');
        $xml = new \SimpleXMLElement($xmlContent);

        foreach ($xml->Hotel as $hotel) {
            Hotel::updateOrCreate(
                ['id' => $hotel->attributes()->id],
                [
                    'name' => $hotel->Name,
                ],
            );
        }

         //echo '\nImportação de hotéis concluída';
    }

    public function importRooms(/* Request $request */)
    {
        $xmlContent = file_get_contents('xmls/rooms.xml');

        $xml = new \SimpleXMLElement($xmlContent);

        foreach ($xml->Room as $room) {
            Room::updateOrCreate(
                ['id' => $room->attributes()->id],
                [
                    'hotel_id' => $room->attributes()->hotelCode,
                    'name' => $room->Name,
                ],
            );
        }

        // echo '\nImportação de Quartos concluída';
    }

    public function importReserves(/* Request $request */)
    {
        $xmlContent = file_get_contents('xmls/reserves.xml');
        $xml = new \SimpleXMLElement($xmlContent);

        foreach ($xml->Reserve as $reserve) {
            $savedReserve = Reserve::updateOrCreate(
                ['id' => $reserve->attributes()->id],
                [
                    'hotel_id' => $reserve->attributes()->hotelCode,
                    'room_id' => $reserve->attributes()->roomCode,

                    'total' => $reserve->Total,
                    'checkIn' => $reserve->CheckIn,
                    'checkOut' => $reserve->CheckOut,
                ],
            );

            foreach ($reserve->Guests->Guest as $guest) {
                // $this->importGuest($guest);

                $savedGuest = Guest::updateOrCreate(['name' => $guest->Name, 'lastName' => $guest->LastName], ['phone' => $guest->Phone]);

                $savedReserve->guests()->attach($savedGuest->id);
            }

            // echo 'Importação de Hospedes concluída';

            foreach ($reserve->Dailies->Daily as $dailyData) {
                $daily = new Daily();

                $daily->date = $dailyData->Date;
                $daily->reserve_id = $savedReserve->id;

                $daily->save();
            }

            // echo 'Importação de Diárias concluída';

            if ($reserve->Payments) {
                foreach ($reserve->Payments->Payment as $paymentData) {
                    $payment = new Payment();

                    $payment->value = $paymentData->Value;
                    $payment->method = $paymentData->Method;
                    $payment->reserve_id = $savedReserve->id;

                    $payment->save();
                }

                // echo 'Importação de Pagamentos concluída';
            }
        }

        // echo 'Importação de Reservas concluída';
    }
}
