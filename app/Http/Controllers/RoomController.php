<?php

namespace App\Http\Controllers;

use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller
{
/**
 * @OA\Get(
 *     path="/api/rooms",
 *     summary="Listar todos os quartos",
 *     description="Este endpoint retorna a lista de todos os quarto disponíveis.",
 *     @OA\Response(response=200, description="Retorna a lista de todas os quartos")
 * )
 */
    public function list()
    {
        $rooms = Room::all();
        return $rooms->toJson();
    }

/**
 * @OA\Post(
 *     path="/api/rooms",
 *     summary="Criar um novo quarto",
 *     description="Este endpoint permite criar um novo quarto com base nos dados fornecidos.",
 *     @OA\RequestBody(
 *         @OA\JsonContent(
 *             type="object",
 *             required={"name", "hotel_id"},
 *             @OA\Property(property="name", type="string", description="Nome do quarto"),
 *             @OA\Property(property="hotel_id", type="integer", description="ID do hotel ao qual o quarto pertence")
 *         )
 *     ),
 *     @OA\Response(response=201, description="quarto criada com sucesso"),
 *     @OA\Response(response=400, description="Requisição inválida")
 * )
 */


    public function create(Request $request)
    {
        $request->validate(Room::$rules);

        $room = Room::create($request->all());
        return $room->toJson();
    }
/**
 * @OA\Get(
 *     path="/api/rooms/{id}",
 *     summary="Obter quarto por ID",
 *     description="Este endpoint retorna um quarto com base no ID fornecido.",
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="ID do quarto",
 *         required=true,
 *         @OA\Schema(type="integer")
 *     ),
 *     @OA\Response(response=200, description="Retorna o quarto com sucesso"),
 *     @OA\Response(response=404, description="quarto não encontrado")
 * )
 */
    public function getById($id)
    {
        $room = Room::find($id);
        if (!$room) {
            return response()->json(['message' => 'Room not found'], 404);
        }
        return $room->toJson();
    }
    /**
 * @OA\Put(
 *     path="/api/rooms/{id}",
 *     summary="Editar quarto por ID",
 *     description="Este endpoint permite editar um quarto com base no ID fornecido e nos dados de atualização.",
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="ID do quarto a ser editada",
 *         required=true,
 *         @OA\Schema(type="integer")
 *     ),
 *     @OA\RequestBody(
 *         @OA\JsonContent(
 *             type="object",
 *             properties={
 *                 @OA\Property(property="name", type="string", description="Novo nome do quarto"),
 *                 @OA\Property(property="hotel_id", type="integer", description="ID do hotel associado o quarto"),
 *             }
 *         )
 *     ),
 *     @OA\Response(response=200, description="quarto editado com sucesso"),
 *     @OA\Response(response=404, description="quarto não encontrado")
 * )
 */

    public function edit(Request $request, $id)
    {
        $room = Room::find($id);

        if (!$room) {
            return response()->json(['message' => 'Room not found'], 404);
        }

        $room->name = is_null($request->name) ? $room->name : $request->name;
        $room->hotel_id = is_null($request->hotel_id) ? $room->hotel_id : $request->hotel_id;

        $room->save();

        return $room->toJson();
    }
/**
 * @OA\Delete(
 *     path="/api/rooms/{id}",
 *     summary="Excluir quarto por ID",
 *     description="Este endpoint permite excluir um quarto com base no ID fornecido, desde que não esteja associada a nenhuma reserva.",
 *     @OA\Parameter(
 *         name="id",
 *         in="path",
 *         description="ID do quarto a ser excluído",
 *         required=true,
 *         @OA\Schema(type="integer")
 *     ),
 *     @OA\Response(response=200, description="quarto excluído com sucesso"),
 *     @OA\Response(response=404, description="quarto não encontrado"),
 *     @OA\Response(response=400, description="Não é possível excluir o quarto, pois está associada a reservas")
 * )
 */
    public function destroy($id)
    {
        $room = Room::find($id);
        if (!$room) {
            return response()->json(['message' => 'Room not found'], 404);
        }

        if ($room->reserves()->exists()) {
            return response()->json(['message' => 'Cannot delete the room as it is associated with reservations:'], 400);
        }

        $room->hotel()->dissociate();

        $room->delete();

        return response()->json(['message' => 'Room deleted successfully']);
    }
}
