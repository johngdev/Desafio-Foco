<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use OpenApi\Annotations as OA;

/**
 * @OA\Info(
 *     title="API Hotelaria - Guia de Documentação",
 *     version="0.1",
 *      @OA\Contact(
 *          email="johnlennongaldino77@gmail.com"
 *      ),
 * ),
 *  @OA\Server(
 *      description="Learning env",
 *      url="http://localhost/api/"
 *  ),
 */

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
}
