<?php

namespace App\Console\Commands;

use App\Http\Controllers\XmlImportController;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;

class ImportXmls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:import-xmls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import the XMLs file to update the database';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $controller = new XmlImportController();

            $controller->importHotels();
            $controller->importRooms();
            $controller->importReserves();

            echo 'Importação concluída';
            DB::commit();
        } catch (\Throwable $th) {
            DB::rollBack();
            echo 'Ocorreu um erro durante a importação';
            echo $th->getMessage();
        }
    }
}
