<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    protected $fillable = ['name', 'lastName', 'phone'];

    public function reserves()
    {
        return $this->belongsToMany(Reserve::class);
    }
}
