<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['method', 'value', 'reserve_id'];

    static public $arrayRules = [
        'payments' => 'required|array',
        'payments.*.method'=> 'required|integer|min:0',
        'payments.*.value'=> 'required|numeric|min:0'
    ];

    public function reserve()
    {
        return $this->belongsTo(Reserve::class);
    }
}
