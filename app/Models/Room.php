<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['hotel_id', 'name'];

    public static $rules = [
        'name' => 'required|string',
        'hotel_id' => 'required|exists:App\Models\Hotel,id',
    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function reserves()
    {
        return $this->hasMany(Reserve::class);
    }
}
