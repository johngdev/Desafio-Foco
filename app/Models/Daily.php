<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Daily extends Model
{
    // tem uma chave composta entre date (o dia) e reserve_id (a reserva)
    // para descrever o pagamento de cada dia
    protected $fillable = ['date', 'reserve_id'];

    public static $rules = [
        'dailies' => 'required|array',
        'date' => 'required|date',
        // 'reserve_id' => 'required|numeric|min:0|exists:App\Models\Reserve,id',
    ];

    public static $arrayRules = ['dailies.*.date' => 'required|date'];
}
