<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reserve extends Model
{
    protected $fillable = ['hotel_id', 'room_id', 'checkIn', 'checkOut', 'total'];

    static public $rules = [
        'hotel_id'=> 'required|exists:App\Models\Hotel,id',
        'room_id'=> 'required|int|exists:App\Models\Room,id',
        'checkIn'=> 'required|date',
        'checkOut'=> 'date|after:checkIn'
    ];

    public function guests()
    {
        return $this->belongsToMany(Guest::class);
    }

    public function dailies()
    {
        return $this->hasMany(Daily::class);
    }

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public function hotel()
    {
        return $this->belongsTo(Hotel::class);
    }

    public function room()
    {
        return $this->belongsTo(Room::class);
    }
}
