# DESAFIO FOCO

  Essa API foi criada para informatizar o sistema de hotéls, gerenciando os quartos/acomodações e reservas, utilizando o padrão REST.
  
  ![swagger](images/swagger.png)

## Requisitos

- [ ] Versão do PHP 8.2.11.
- [ ] Banco de dados MySQL.
- [ ] Composer instalado.
- [ ] Laravel Framework 10.28.0

## Ferramentas

- Artisan: poderosa CLI para gerenciar a aplicação.
- Eloquent: ORM para mapeamento de dados com o banco.
- MySql: SGBD Relacional utilizado na aplicação. 
- Swagger: domuntação utilizando o padrão OpenApi.
    - Utilização de comando para gerar a documentação (`swagger.json`)

## Como executar o projeto
para iniciar o projeto voce deve, depois de garantir que o banco de dados esteja online e configurado:

1. Executar as migratinos

  ```bash 
  php artisan migrate
  ```

2. Iniciar a Aplicação

  ```bash 
  php artisan migrate
  ``` 

## Detalhes do projeto

### Controlllers

Métodos chamados pelo sistema de roteamento do Laravel

- `XmlImportController`: controller responsável por importar os XMLs contendo dados que devem ser salvos em banco
- `RoomController`: controller com métodos CRUD completo para gerenciar Rooms (quartos/acomadações dos hotéis)
- `ReserveController`: controller para adicionar e listar Reservas no sistema, além de criar Diárias e Pagamentos atrelados àquela reserva

### Rotas API Rest

As rotas foram criadas no arquivo api.php

O padrão é consistente e segue as melhores práticas para definir rotas no Laravel, tornando a API clara e fácil de entender. Cada rota corresponde a uma ação específica em um controller.

```plain
// Rooms

Route::get('/rooms', 'App\Http\Controllers\RoomController@list');
Route::post('/rooms', 'App\Http\Controllers\RoomController@create');
Route::get('/rooms/{id}', 'App\Http\Controllers\RoomController@getById');
Route::patch('/rooms/{id}', 'App\Http\Controllers\RoomController@edit');
Route::delete('/rooms/{id}', 'App\Http\Controllers\RoomController@destroy');

// Reserves

Route::get('/reserves', 'App\Http\Controllers\ReserveController@list');
Route::post('/reserves', 'App\Http\Controllers\ReserveController@createReserve');
Route::post('/reserves/{reserveId}/dailies', 'App\Http\Controllers\ReserveController@addDailies');
Route::post('/reserves/{reserveId}/payments', 'App\Http\Controllers\ReserveController@addPayments');
```

### Models

As models representam as entidades do negócio e alguns de seus detalhes. 

#### Validação

Algumas models trazem consigo as regras de validação para garantir a integradidade dos dados do projeto. As regras de validação foram postas dentro do model para compor a entidade e, assim, mantê-la coesa.

```php
class Reserve extends Model
{
    protected $fillable = ['hotel_id', 'room_id', 'checkIn', 'checkOut', 'total'];

    static public $rules = [
        'hotel_id'=> 'required|exists:App\Models\Hotel,id',
        'room_id'=> 'required|int|exists:App\Models\Room,id',
        'checkIn'=> 'required|date',
        'checkOut'=> 'date|after:checkIn'
    ];

    // outros métodos e propriedades ...
}
```

### Banco de dados

Como banco de dados foi escolhido o MySql pela fácil configuração com o Eloquent junto ao Laravel

![dabase](images/banco.png)

##### Migrations

As migrations foram usadas para especificar as mudanças no esquema do banco durante o desenvolvimento

- As migrations criadas inicialmente criaram as principais tables:
  - `Rooms`
  - `Guests`
  - `Hotels`
  - `Reserves`
  - `Payments`
  - `Dailies` 
- Outras migrations foram necessárias para aprimorar e corrigir o esquema do banco ao decorrer do desenvolvimento, como:
  - Adicionar ids a algumas tabelas
  - Criar chaves compostas

### Comandos

Temos acesso a definir Commands que podem ser executados a partir do agendador do Laravel ou a partir do CLI `artisan`

Atualmente, o projeto é composto por dois comandos:

- `app:import-xmls`: usa o controlador XmlImportController para importar os dados de XMLs presentes na aplicação
- `swagger`: gera a documentação do Swagger para API

#### Invocando comandos com cron

Para disparar os Commands voce pode executar o seguinte comando em seu terminal:

```bash
php artisan <assinatura_do_comando>
```

então, para invocá-los com CRON voce pode executar o script acima como parametro do comando CRON

```bash
* * * * * cd /path-to-your-project && php artisan <assinatura_do_comando>
```

---

Obrigado.
