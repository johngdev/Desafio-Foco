<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// XML action




// Rooms

Route::get('/rooms', 'App\Http\Controllers\RoomController@list');
Route::post('/rooms', 'App\Http\Controllers\RoomController@create');
Route::get('/rooms/{id}', 'App\Http\Controllers\RoomController@getById');
Route::patch('/rooms/{id}', 'App\Http\Controllers\RoomController@edit');
Route::delete('/rooms/{id}', 'App\Http\Controllers\RoomController@destroy');

// Reserves

Route::get('/reserves', 'App\Http\Controllers\ReserveController@list');
Route::post('/reserves', 'App\Http\Controllers\ReserveController@createReserve');
Route::post('/reserves/{reserveId}/dailies', 'App\Http\Controllers\ReserveController@addDailies');
Route::post('/reserves/{reserveId}/payments', 'App\Http\Controllers\ReserveController@addPayments');

