<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('dailies', function (Blueprint $table) {

        $table->date('date');
        $table->unsignedBigInteger('reserve_id');

        $table->foreign('reserve_id')->references('id')->on('reserves');

        $table->unique(['date', 'reserve_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('dailies', function (Blueprint $table) {
            $table->dropForeign(['reserve_id']); // Remove a chave estrangeira
            $table->dropUnique(['date', 'reserve_id']); // Remove o índice de unicidade
        });
    }
};
