<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('guests', function (Blueprint $table) {
            $table->dropForeign(['reserve_id']);
            $table->dropColumn('reserve_id');
        });

        Schema::create('guest_reserve', function (Blueprint $table) {
            $table->unsignedBigInteger('reserve_id');
            $table->unsignedBigInteger('guest_id');
            $table
                ->foreign('reserve_id')
                ->references('id')
                ->on('reserves')
                ->onDelete('cascade');
            $table
                ->foreign('guest_id')
                ->references('id')
                ->on('guests')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guest_reserve');

        Schema::table('guests', function (Blueprint $table) {
            $table->unsignedBigInteger('reserve_id');
            $table->foreign('reserve_id')->references('id')->on('reserves');
        });
    }
};
