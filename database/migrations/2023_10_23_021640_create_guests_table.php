<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('guests', function (Blueprint $table) {
            $table->unsignedBigInteger('reserve_id');
            $table->string('name');
            $table->string('lastName');
            $table->string('phone');
            $table->timestamps();
            $table->foreign('reserve_id')->references('id')->on('reserves');
        });
    }


    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('guests');
    }
};
